---
layout: default
title: Areas of Focus
---
<div class='search text-center'>
  <input type='text' id='searchbox' name='searchbox' placeholder='Enter search term here' />
</div>
<hr />
{% assign total = site.members.size | times: 1.0 %}
{% assign max   = 100 %}
{% assign sm    = 0 %}
{% assign saas  = 0 %}
{% assign ops   = 0 %}
{% assign lnr   = 0 %}
{% assign lead  = 0 %}
{% assign fed   = 0 %}
{% for m in site.members %}
  {% for f in m.focuses %}
    {% if f.name == 'Self-Managed' %}
      {% assign sm = sm | plus: f.percentage %}
    {% elsif f.name == 'SaaS'%}
      {% assign saas = saas | plus: f.percentage %}
    {% elsif f.name == 'License and Renewals' %}
      {% assign lnr = lnr | plus: f.percentage %}
    {% elsif f.name == 'Operations' %}
      {% assign ops = ops | plus: f.percentage %}
    {% elsif f.name == 'US Federal' %}
      {% assign fed = fed | plus: f.percentage %}
    {% else %}
      {% assign lead = lead | plus: f.percentage %}
    {% endif %}
  {% endfor %}
{% endfor %}
{% assign sm   = sm | divided_by: total %}
{% assign saas = saas | divided_by: total %}
{% assign ops  = ops | divided_by: total %}
{% assign lnr  = lnr | divided_by: total %}
{% assign lead = lead | divided_by: total %}
{% assign fed  = fed | divided_by: total %}

<div id='total-areas-of-focus'>
  <div class='progress'>
    <div class="progress-bar bg-success" role="progressbar" style="width: {{ sm }}%" aria-valuenow="{{ sm }}" aria-valuemin="0" aria-valuemax="{{ max }}">SM</div>
    <div class="progress-bar bg-info" role="progressbar" style="width: {{ saas }}%" aria-valuenow="{{ saas }}" aria-valuemin="0" aria-valuemax="{{ max }}">SaaS</div>
    <div class="progress-bar bg-warning" role="progressbar" style="width: {{ lnr }}%" aria-valuenow="{{ lnr }}" aria-valuemin="0" aria-valuemax="{{ max }}">L&R</div>
    <div class="progress-bar bg-danger" role="progressbar" style="width: {{ ops }}%" aria-valuenow="{{ ops }}" aria-valuemin="0" aria-valuemax="{{ max }}">Ops</div>
    <div class="progress-bar bg-primary" role="progressbar" style="width: {{ lead }}%" aria-valuenow="{{ lead }}" aria-valuemin="0" aria-valuemax="{{ max }}">Mgr</div>
    <div class="progress-bar bg-secondary" role="progressbar" style="width: {{ fed }}%" aria-valuenow="{{ fed }}" aria-valuemin="0" aria-valuemax="{{ max }}">USFed</div>
  </div>
  <ul>
    <li>Self-Managed: {{ sm | round: 2 }}%</li>
    <li>SaaS: {{ saas | round: 2 }}%</li>
    <li>License and Renewals: {{ lnr | round: 2 }}%</li>
    <li>US Federal: {{ fed | round: 2 }}%</li>
    <li>Management: {{ lead | round: 2 }}%</li>
    <li>Operations: {{ ops | round: 2 }}%</li>
  </ul>
</div>

<hr>

<table class='table table-hover table-striped'>
  <thead class='thead thead-dark'>
    <tr>
      <th>Name</th>
      <th class='text-center'>Focus</th>
    </tr>
  </thead>
  <tbody>
    {% for m in site.members %}
    {% assign sm   = m.focuses | where: "name", "Self-Managed" %}
    {% assign saas = m.focuses | where: "name", "SaaS" %}
    {% assign ops  = m.focuses | where: "name", "Operations" %}
    {% assign lnr  = m.focuses | where: "name", "License and Renewals" %}
    {% assign lead = m.focuses | where: "name", "Management" %}
    {% assign fed  = m.focuses | where: "name", "US Federal" %}
    <tr>
      <td>{{ m.name }}</td>
      <td>
        <div class='progress'>
          {% if sm.size == 1 %}
          <div class="progress-bar bg-success" role="progressbar" style="width: {{ sm.first.percentage }}%" aria-valuenow="{{ sm.first.percentage }}" aria-valuemin="0" aria-valuemax="100">SM - {{ sm.first.percentage }}%</div>
          {% endif %}
          {% if saas.size == 1 %}
          <div class="progress-bar bg-info" role="progressbar" style="width: {{ saas.first.percentage }}%" aria-valuenow="{{ saas.first.percentage }}" aria-valuemin="0" aria-valuemax="100">SaaS - {{ saas.first.percentage }}%</div>
          {% endif %}
          {% if lnr.size == 1 %}
          <div class="progress-bar bg-warning" role="progressbar" style="width: {{ lnr.first.percentage }}%" aria-valuenow="{{ lnr.first.percentage }}" aria-valuemin="0" aria-valuemax="100">L&R - {{ lnr.first.percentage }}%</div>
          {% endif %}
          {% if ops.size == 1 %}
          <div class="progress-bar bg-danger" role="progressbar" style="width: {{ ops.first.percentage }}%" aria-valuenow="{{ ops.first.percentage }}" aria-valuemin="0" aria-valuemax="100">Ops - {{ ops.first.percentage }}%</div>
          {% endif %}
          {% if lead.size == 1 %}
          <div class="progress-bar bg-primary" role="progressbar" style="width: {{ lead.first.percentage }}%" aria-valuenow="{{ lead.first.percentage }}" aria-valuemin="0" aria-valuemax="100">Mgr - {{ lead.first.percentage }}%</div>
          {% endif %}
          {% if fed.size == 1 %}
          <div class="progress-bar bg-secondary" role="progressbar" style="width: {{ fed.first.percentage }}%" aria-valuenow="{{ fed.first.percentage }}" aria-valuemin="0" aria-valuemax="100">USFed - {{ fed.first.percentage }}%</div>
          {% endif %}
        </div>
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>
