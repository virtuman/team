$(function() {
  var split = location.search.replace('?', '').split('&').map(function(val){
    if (val.split('=')[0] == 'search') {
      return val.split('=')[1]
    }
  })[0];
  if (split != '' && split != null) {
    var search = split.toLowerCase();
    $('#searchbox').val(split);
    $('tbody > tr').each(function() {
      var show = false;
      $(this).children('td').each(function() {
        var text = $(this).text().toLowerCase();
        if (text.indexOf(search) >= 0) {
          show = true;
        }
      });
      if (show) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  }
});
$('#searchbox').keyup(function() {
  var search = $(this).val().toLowerCase();
  if (search == '') {
    $('tbody > tr').show();
  } else {
    $('tbody > tr').each(function() {
      var show = false;
      $(this).children('td').each(function() {
        var text = $(this).text().toLowerCase();
        if (text.indexOf(search) >= 0) {
          show = true;
        }
      });
      if (show) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  }
});
