#!/usr/bin/env ruby
# frozen_string_literal: true

require 'active_support/time'
require 'faraday'
require 'oj'
require 'yaml'

def schedules
  {
    'Customer Emergencies AMER': 'PIQ317K',
    'Customer Emergencies APAC 1': 'PQB9Q6K',
    'Customer Emergencies APAC 2': 'PKPXM8K', 'US Federal': 'P89ZYHZ',
    'Customer Emergencies EMEA': 'P9SV029', 'FRT AMER East': 'PNNCL8O',
    'FRT AMER West': 'PFR91E4', 'CMOC AMER': 'PG0SHU2',
    'CMOC APAC 1': 'PGUP5OB', 'CMOC APAC 2': 'PMPKHZN', 'CMOC EMEA': 'P59382D',
    'SLA AMER East': 'PARKBKA', 'SLA AMER West': 'PB3SHMC',
    'SLA APAC 1': 'PDOE49N', 'SLA APAC 2': 'PDWWNBQ', 'SLA EMEA': 'PNGTKL5'
  }
end

def gitlab_client
  Faraday.new('https://gitlab.com/api/v4') do |c|
    c.request :url_encoded
    c.adapter Faraday.default_adapter
    c.headers['Private-Token'] = ENV['GL_TOKEN']
  end
end

def gitlab_request(http_method, endpoint, params = {})
  response = gitlab_client.public_send(http_method, endpoint, params)
  Oj.load(response.body)
end

def pd_client
  Faraday.new('https://api.pagerduty.com') do |c|
    c.request :url_encoded
    c.adapter Faraday.default_adapter
    c.headers['Authorization'] = "Token token=#{ENV['PD_TOKEN']}"
    c.headers['Content-Type'] = 'application/json'
    c.headers['Accept'] = 'application/vnd.pagerduty+json;version=2'
  end
end

def pd_request(http_method, endpoint, params = {})
  response = pd_client.public_send(http_method, endpoint, params)
  Oj.load(response.body)
end

def current_start_date
  if Date.today.wday == 1
    Date.today
  else
    Date.today.prev_occurring(:monday)
  end
end

def current_end_date
  Date.today.next_occurring(:sunday)
end

def next_start_date
  if Date.today.wday.zero?
    Date.today.next_occurring(:monday) + 7
  else
    Date.today.next_occurring(:monday)
  end
end

def next_end_date
  Date.today.next_occurring(:sunday) + 7
end

def current_pd_url(id)
  "schedules/#{id}?time_zone=UTC&since=#{current_start_date}&until=#{current_end_date}"
end

def next_pd_url(id)
  "schedules/#{id}?time_zone=UTC&since=#{next_start_date}&until=#{next_end_date}"
end

def support_pairings(per)
  obj = []
  id = per['gitlab']['id']
  opts = "state=closed&assignee_id=#{id}&per_page=100&order_by=updated_at&sort=desc"
  res = gitlab_request(:get, "projects/14978605/issues?#{opts}")
  res.each do |r|
    obj.push(r['closed_at'])
  end
  process_pairings(per, obj)
end

def process_pairings(per, dates)
  total = dates.count
  last7 = 0
  last30 = 0
  dates.each do |d|
    diff = (Time.now - Time.parse(d)) / 86400
    if diff <= 7
      last7 += 1
      last30 += 1
    elsif diff <= 30
      last30 += 1
    end
  end
  { name: per['name'],
    total: (total == 100 ? '<=100' : total),
    last7: last7,
    last30: (last30 == 100 ? '<=100' : last30) }
end

bootcamps = {}
skills    = {}
pairings  = {}
yaml      = YAML.load_file('data/support-team.yaml')

yaml.each do |p|
  slug = p['email'].split('@').first
  File.open("_members/#{slug}.yaml", 'w') { |f| f.write(p.to_yaml) }
  p['bootcamps'].each do |b|
    bootcamps[b] ||= []
    bootcamps[b].push(p['name'])
  end
  p['knowledge_areas'].each do |k|
    skills[k] ||= []
    skills[k].push(p['name'])
  end
end
bootcamps.each do |name, people|
  out = "---\nname: #{name}\npeople:\n"
  people.each do |p|
    out += "- #{p}\n"
  end
  File.open("_bootcamps/#{name}.yaml", 'w') { |f| f.write(out) }
end
skills.each do |name, people|
  out = "---\nname: #{name}\npeople:\n"
  people.each do |p|
    out += "- #{p}\n"
  end
  File.open("_skills/#{name}.yaml", 'w') { |f| f.write(out) }
end
schedules.each do |(name, id)|
  res = pd_request(:get, current_pd_url(id))
  out = "---\nname: #{name}\nschedule:\n  current:\n"
  people = []
  res['schedule']['final_schedule']['rendered_schedule_entries'].each do |s|
    people.push(s['user']['summary'])
  end
  people.uniq.each { |p| out += "  - #{p}\n" }
  res = pd_request(:get, next_pd_url(id))
  people = []
  out += "  next:\n"
  res['schedule']['final_schedule']['rendered_schedule_entries'].each do |s|
    people.push(s['user']['summary'])
  end
  people.uniq.each { |p| out += "  - #{p}\n" }
  File.open("_oncall/#{name}.yaml", 'w') { |f| f.write(out) }
end
yaml.each do |m|
  slug = m['email'].split('@').first
  p = support_pairings(m)
  out = "---\nname: #{p[:name]}\ntotal: #{p[:total]}\nlast7: #{p[:last7]}\nlast30: #{p[:last30]}"
  File.open("_pairings/#{slug}.yaml", 'w') { |f| f.write(out) }
end
