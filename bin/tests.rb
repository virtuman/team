#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'

def regions
  %w[
    AMER-W
    AMER-C
    AMER-E
    APAC
    EMEA
  ]
end

def test_region(per)
  return if regions.include? per['region']

  abort("Bad Region for #{per['name']}: #{per['region']}")
end

def oncall # rubocop:disable Metrics/MethodLength
  [
    'CMOC AMER',
    'CMOC APAC 1',
    'CMOC APAC 2',
    'CMOC EMEA',
    'Customer Emergencies AMER',
    'Customer Emergencies APAC 1',
    'Customer Emergencies APAC 2',
    'Customer Emergencies EMEA',
    'FRT AMER East',
    'FRT AMER West',
    'SLA AMER East',
    'SLA AMER West',
    'SLA APAC 1',
    'SLA APAC 2',
    'SLA EMEA',
    'US Federal',
    'US Federal Shadow',
    'SSAT',
    'Support Manager AMER',
    'Support Manager APAC',
    'Support Manager EMEA'
  ]
end

def test_pagerduty(per)
  per['pagerduty']['rotations'].each do |r|
    abort("Bad PD rotation for #{per['name']}: #{r}") unless oncall.include? r
  end
end

def zd_main_groups
  [
    'General',
    'Support AMER',
    'Support APAC',
    'Support EMEA',
    'Support Managers',
    'Support Ops',
    'Tkt Management 1'
  ]
end

def zd_main_roles
  [
    'Support Staff',
    'Support Staff - Explore',
    'Support Managers',
    'Administrator'
  ]
end

def test_zendesk_main(per)
  zendesk = per['zendesk']['main']
  test_zd_main_groups(per)
  # rubocop:disable Style/GuardClause
  unless zd_main_roles.include? zendesk['role']
    abort("Bad ZD Main role for #{per['name']}: #{zendesk['role']}")
  end
  # rubocop:enable Style/GuardClause
end

def test_zd_main_groups(per)
  zendesk = per['zendesk']['main']
  if zendesk['groups'].count.zero?
    puts "!!! #{per['name']} is not in any groups for Zendesk Main !!!"
  end
  zendesk['groups'].each do |g|
    unless zd_main_groups.include? g
      abort("Bad ZD Main group for #{per['name']}: #{g}")
    end
  end
end

def zd_usfed_groups
  [
    'Support'
  ]
end

def zd_usfed_roles
  [
    'Administrator',
    'Support US Federal Staff'
  ]
end

def test_zendesk_usfed(per)
  zendesk = per['zendesk']['us-federal']
  test_zd_usfed_groups(per)
  # rubocop:disable Style/GuardClause
  unless zd_usfed_roles.include? zendesk['role']
    abort("Bad ZD USFed role for #{per['name']}: #{zendesk['role']}")
  end
  # rubocop:enable Style/GuardClause
end

def test_zd_usfed_groups(per)
  zendesk = per['zendesk']['us-federal']
  if zendesk['groups'].count.zero?
    puts "!!! #{per['name']} is not in any groups for Zendesk USFed"
  end
  zendesk['groups'].each do |g|
    unless zd_usfed_groups.include? g
      abort("Bad ZD USFed group for #{per['name']}: #{g}")
    end
  end
end

def bootcamps # rubocop:disable Metrics/MethodLength
  [
    'API',
    'CI-CD',
    'CMOC',
    'Elasticsearch',
    'Geo',
    'GitLab Pages',
    'GitLab SSL',
    'High Availability',
    'Kubernetes',
    'LDAP',
    'Omnibus',
    'SAML',
    'Service Desk',
    'Web Accessibility'
  ]
end

def test_bootcamps(per)
  per['bootcamps'].each do |b|
    abort("Bad Bootcamp for #{per['name']}: #{b}") unless bootcamps.include? b
  end
end

def areas # rubocop:disable Metrics/MethodLength
  [
    'Ansible',
    'Backup-Restore',
    'Bash',
    'C',
    'C++',
    'CI',
    'Cloud Native',
    'Code Owners',
    'Composition Analysis',
    'Container Registry',
    'Database Migrations',
    'Dependency Proxy',
    'Docker',
    'Dynamic Analysis',
    'Elasticsearch',
    'Geo',
    'Git LFS',
    'GitLab Pages',
    'GitLab Runner',
    'GitLab SSL',
    'Gitaly',
    'Go',
    'Helm',
    'High Availability',
    'Issue Tracking',
    'JIRA Integration',
    'Javascript',
    'Jenkins Integration',
    'Kubernetes',
    'Kubernetes Executor',
    'LDAP',
    'License and Renewals',
    'Linux',
    'Metrics',
    'Monitoring GitLab',
    'Omnibus',
    'Package',
    'Performance',
    'Perl',
    'PostgreSQL',
    'Project Import',
    'Python',
    'Repository Mirroring',
    'Ruby',
    'Ruby on Rails',
    'SAML',
    'SSO for Groups',
    'Service Desk',
    'Snippets',
    'Static Analysis',
    'Technical Writing',
    'Vue.js',
    'WAF',
    'Web IDE',
    'Windows',
    'Zendesk Administration',
    'Zendesk App Development',
    'jQuery'
  ]
end

def test_knowledge_areas(per)
  per['knowledge_areas'].each do |k|
    abort("Bad Knowledge Area for #{per['name']}: #{k}") unless areas.include? k
  end
end

begin
  members = YAML.load_file('data/support-team.yaml')
rescue Psych::SyntaxError
  abort("Invalid Syntax: #{file}")
end
members.each do |m|
  test_region(m)
  test_pagerduty(m)
  if m['zendesk']['main']['id'].nil?
    puts "!!! No ZD Main for #{m['name']} !!!"
    test_zendesk_main(m)
  end
  test_zendesk_usfed(m) unless m['zendesk']['us-federal']['id'].nil?
  test_bootcamps(m)
  test_knowledge_areas(m)
end
