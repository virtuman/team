---
layout: default
title: Oncall Schedules
---
<div class='search text-center'>
  <input type='text' id='searchbox' name='searchbox' placeholder='Enter search term here' />
</div>
<hr />
<table class='table table-striped table-hover'>
  <thead class='thead thead-dark'>
    <tr>
      <th>Schedule</th>
      <th>Currently Oncall</th>
      <th>Oncall Next Week</th>
    </tr>
  </thead>
  <tbody>
    {% for pd in site.oncall %}
    <tr>
      <td class='align-middle'>{{ pd.name }}</td>
      <td>
        <ul>
          {% for p in pd.schedule.current %}
          <li>{{ p }}</li>
          {% endfor %}
        </ul>
      </td>
      <td>
        <ul>
          {% for p in pd.schedule.next %}
          <li>{{ p }}</li>
          {% endfor %}
        </ul>
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>
