---
layout: default
title: Support Pairings
---
<div class='search text-center'>
  <input type='text' id='searchbox' name='searchbox' placeholder='Enter search term here' />
</div>
<hr />
<hr>
<table class='table table-striped table-hover'>
  <thead class='thead thead-dark'>
    <tr>
      <th>Name</th>
      <th class='text-center'>Last 7 Days</th>
      <th class='text-center'>Last 30 Days</th>
      <th class='text-center'>Total</th>
    </tr>
  </thead>
  <tbody>
    {% for p in site.pairings %}
    <tr>
      <td>{{ p.name }}</td>
      <td class='align-middle text-center'>{{ p.last7 }}</td>
      <td class='align-middle text-center'>{{ p.last30 }}</td>
      <td class='align-middle text-center'>{{ p.total }}</td>
    </tr>
    {% endfor %}
 </tbody>
</table>
