---
layout: default
title: Personal Info
---
<div class='search text-center'>
  <input type='text' id='searchbox' name='searchbox' placeholder='Enter search term here' />
</div>
<hr />
<table class='table table-hover table-striped'>
  <thead class='thead thead-dark'>
    <tr>
      <th>Name</th>
      <th>Calendly</th>
      <th>Location</th>
      <th>Hobbies</th>
      <th>Birthdate</th>
    </tr>
  </thead>
  <tbody>
    {% for p in site.members %}
    <tr>
      <td class='align-middle'>{{ p.name }}</td>
      {% if p.calendly %}
      <td class='align-middle'><a href='{{ p.calendly }}' target='_blank'>Calendly</a></td>
      {% else %}
      <td></td>
      {% endif %}
      <td class='align-middle'>{{ p.location }}, {{ p.country }}</td>
      <td class='align-middle'>
        <ul>
          {% for h in p.hobbies %}
          <li>{{ h }}</li>
          {% endfor %}
        </ul>
      </td>
      <td class='align-middle'>{{ p.birthday }}</td>
    </tr>
    {% endfor %}
  </tbody>
</table>
