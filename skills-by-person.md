---
layout: default
title: Skills by Person
---
<div class='search text-center'>
  <input type='text' id='searchbox' name='searchbox' placeholder='Enter search term here' />
</div>
<hr />
<table class='table table-striped table-hover'>
  <thead class='thead thead-dark'>
    <tr>
      <th>Name</th>
      <th>Region</th>
      <th>Bootcamps</th>
      <th>Knowledge Areas</th>
    </tr>
  </thead>
  <tbody>
    {% for m in site.members %}
    {% if m.bootcamps.size > 0 or m.knowledge_areas.size > 0 %}
    <tr>
      <td class='align-middle'>{{ m.name }}</td>
      <td class='align-middle'>{{ m.region }}</td>
      <td>
        <ul>
          {% for b in m.bootcamps %}
          <li>{{ b }} Bootcamp</li>
          {% endfor %}
        </ul>
      </td>
      <td>
        <ul>
          {% for k in m.knowledge_areas %}
          <li>{{ k }}</li>
          {% endfor %}
        </ul>
      </td>
    </tr>
    {% endif %}
    {% endfor %}
  </tbody>
</table>
