---
layout: default
title: Skills by Subject
---
<div class='search text-center'>
  <input type='text' id='searchbox' name='searchbox' placeholder='Enter search term here' />
</div>
<hr />
<table class='table table-striped table-hover'>
  <thead class='thead thead-dark'>
    <tr>
      <th>Name of Subject</th>
      <th>People</th>
    </tr>
  </thead>
  <tbody>
    {% for b in site.bootcamps %}
    <tr>
      <td class='align-middle'>{{ b.name }} Bootcamp</td>
      <td>
        <ul>
        {% for p in b.people %}
        <li>{{ p }}</li>
        {% endfor %}
        </ul>
      </td>
    </tr>
    {% endfor %}
    {% for b in site.skills %}
    <tr>
      <td class='align-middle'>{{ b.name }}</td>
      <td>
        <ul>
        {% for p in b.people %}
        <li>{{ p }}</li>
        {% endfor %}
        </ul>
      </td>
    </tr>
    {% endfor %}
  </tbody>
</table>
